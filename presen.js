var S = {};
//main func
S.init = function(data){
    S.origin = data;
    S.position = 0;
    init_page(S.origin);
    loadData(S.page[S.position]);
}

var init_page = function(data){
    var count = 0;
    var array = data.split("\n");
    var pagearray = new Array();
    var string = "";
    for(var i = 0; i < array.length; i++){
	if(array[i] === "===="){
	    pagearray.push(string);
            count++;
	    string = "";
	    continue;
	}
	string += array[i];
    }
    //set
    S.page = pagearray;
    S.pages = count;
}

S.has_prev = function(){
    return (S.position != 0)
}

S.has_next = function(){
    return (S.position < S.pages - 1);
}

S.prevpage = function(){
    S.position -= 1;
    return S.page[S.position];
}

S.nextpage = function(){
    S.position += 1;
    return S.page[S.position];
}

var loadData = function(str){
    $('#result').html(str);

    setInterval(function(){
	var date = new Date();
	$(".time").text(String(date));
    },1000);

    $(".spos").text((S.position + 1) + "/" + S.pages);
    document.title = '';
    $("#maintitle").html(document.title);
};

$(function(){
    $.get("content.txt",
          S.init,
          "text"
         );

    $(document).keydown(function(evt){
        if(evt.which == 74){
	    if(S.has_next())
		loadData(S.nextpage());
        }
        else if(evt.which == 75){
	    if(S.has_prev())
		loadData(S.prevpage());
        }
    });
});
